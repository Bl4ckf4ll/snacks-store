<?php

namespace App\Observers;

use App\Models\Product;
use App\Models\Product\Action as ProductAction;
use Illuminate\Support\Facades\Auth;

class ProductObserver
{
    /**
     * Admin user instance
     * 
     * @var Admin
     */
    public $admin;

    /**
     * Setup class
     */
    public function __construct()
    {
        $this->admin = Auth::user('admin');
    }

    /**
     * Handle the product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        if (!$this->admin) {
            return $product;
        }

        $action = new ProductAction([
            'action' => 'create',
            'snapshot' => $product->toJson(),
        ]);

        $action->product()->associate($product);
        $action->admin()->associate($this->admin);

        $action->save();

        return $product;
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        if (!$this->admin) {
            return $product;
        }

        $action = new ProductAction([
            'action' => 'update',
            'snapshot' => $product->toJson(),
        ]);

        $action->product()->associate($product);
        $action->admin()->associate($this->admin);

        $action->save();

        return $product;
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        if (!$this->admin) {
            return $product;
        }

        $action = new ProductAction([
            'action' => 'delete',
            'snapshot' => $product->toJson(),
        ]);

        $action->product()->associate($product);
        $action->admin()->associate($this->admin);

        $action->save();

        return $product;
    }
}
