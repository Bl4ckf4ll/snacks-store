<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    /**
     * Fields protected to mass assignment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Like belongs to product relationship
     *
     * @return 
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * Like belongs to customer relationship
     * 
     * @return
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
}
