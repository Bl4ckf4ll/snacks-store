<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Fields protected to mass assignment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Product has many likes relationship
     * 
     * @return
     */
    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    /**
     * Method for buy process
     *
     * @param Customer $customer
     * @param int $quantity
     * @return void
     */
    public function buy(Customer $customer, int $quantity)
    {
        $this->fill([
            'quantity' => $this->quantity - $quantity,
        ])->save();

        $order = new Order([
            'quantity' => $quantity,
            'spent' => $this->price * $quantity,
        ]);

        $order->product()->associate($this);
        $order->customer()->associate($customer);

        $order->save();

        return $this;
    }
}
