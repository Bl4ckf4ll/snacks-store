<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Fields protected to mass assignment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Order belongs to customer relationship
     *
     * @return BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    /**
     * Order belongs to product relationship
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
