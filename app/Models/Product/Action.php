<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    /**
     * DB table for model to use
     *
     * @var array
     */
    protected $table = 'product_actions';

    /**
     * Fields protected to mass assignment
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Action belongs to product relationship
     *
     * @return BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * Action belongs to admin relationship
     * 
     * @return BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }
}
