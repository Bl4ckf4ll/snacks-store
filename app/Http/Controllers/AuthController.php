<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Authenticate admin users
     *
     * @param Request $request
     * @return TokenResponse
     */
    public function admin(Request $request)
    {
        $token = Auth::attempt($request->only('email', 'password'));

        if (!$token) {
            return response([
                'message' => 'Invalid credentials',
            ], 400);
        }

        return $token;
    }

    /**
     * Authenticate customer users
     *
     * @param Request $request
     * @return TokenResponse
     */
    public function customer(Request $request)
    {
        $token = Auth::guard('customer')
            ->attempt($request->only('email', 'password'));

        if (!$token) {
            return response([
                'message' => 'Invalid credentials',
            ], 400);
        }

        return $token;
    }
}
