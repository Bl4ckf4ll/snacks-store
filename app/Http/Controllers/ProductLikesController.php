<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Like;
use Illuminate\Support\Facades\DB;

class ProductLikesController extends Controller
{
    /**
     * Store like for product
     *
     * @param Product $product
     * @return void
     */
    public function store(Product $product)
    {
        $customer = Auth::user('customer');

        if ($product->likes()->where('customer_id', '=', $customer->id)->exists()) {
            return response([
                'message' => 'Already liked product',
            ], 400);
        }

        DB::transaction(function () use ($product, $customer) {
            $like = new Like();

            $like->customer()->associate($customer);
            $like->product()->associate($product);

            $like->save();

            $product->fill([
                'likes_count' => ++$product->likes_count,
            ])->save();
        });

        return $product->load('likes');
    }
}
