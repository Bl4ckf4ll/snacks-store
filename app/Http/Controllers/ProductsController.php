<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\Product as ProductResource;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    /**
     * Return products listing
     *
     * @return void
     */
    public function index(Request $request)
    {
        $orderBy = $request->input('orderBy') === 'popularity'
            ? 'likes_count,DESC'
            : 'name,ASC';
        list(
            $orderColumn,
            $orderType
        ) = explode(',', $orderBy);

        return ProductResource::collection(
            Product::where('name', 'LIKE', "%{$request->input('search')}%")
                ->orderBy($orderColumn, $orderType)
                ->with('likes.customer')
                ->paginate()
        );
    }

    /**
     * Store product
     *
     * @param Request $request
     * @return Product
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:products',
            'description' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);

        return Product::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'quantity' => $request->input('quantity'),
        ]);
    }

    /**
     * Get single product
     *
     * @param Request $request
     * @return Product
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Update product
     *
     * @param Request $request
     * @return Product
     */
    public function update(Request $request, Product $product, string $field = null)
    {
        if ($field) {
            $product->fill([
                $field => $request->input('value'),
            ])->save();

            return $product;
        }

        if ($request->isMethod('put')) {
            $product->update([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'price' => $request->input('price'),
            ]);
        }

        return $product;
    }

    /**
     * Delete product
     *
     * @param Product $product
     * @return Product
     */
    public function delete(Product $product)
    {
        $product->delete();

        return $product;
    }

    /**
     * Simple endpoint for buying a product
     *
     * @param Product $product
     * @return void
     */
    public function buy(Request $request, Product $product)
    {
        $quantity = $request->input('quantity', 1);
        $customer = Auth::user('customer');

        if (($product->quantity - $quantity) < 1) {
            return response([
                'message' => 'Product not available',
            ], 400);
        }

        return DB::transaction(function () use ($product, $quantity, $customer) {
            return $product->buy($customer, $quantity);
        });
    }
}
