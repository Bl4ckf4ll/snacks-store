<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Admin;
use App\Models\Customer;

class AuhtControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test successfull authentication status
     *
     * @return void
     * @test
     */
    public function admin_auth_success_should_have_200_status()
    {
        $password = '123456';

        $user = factory(Admin::class)->create([
            'password' => $password,
        ]);

        $response = $this->json('POST', '/api/auth/admin', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $response->assertStatus(200);
    }

    /**
     * Test successfull authentication response format
     *
     * @return void
     * @test
     */
    public function admin_auth_success_should_match_format()
    {
        $password = '123456';

        $user = factory(Admin::class)->create([
            'password' => $password,
        ]);

        $response = $this->json('POST', '/api/auth/admin', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    /**
     * Test Failed authentication status
     *
     * @return void
     * @test
     */
    public function admin_auth_fails_should_have_400_status()
    {
        $response = $this->json('POST', '/api/auth/admin', [
            'email' => 'bad@email.com',
            'password' => '123456',
        ]);

        $response->assertStatus(400);
    }

    /**
     * Test Failed authentication response format
     *
     * @return void
     * @test
     */
    public function admin_auth_fails_should_match_format()
    {
        $response = $this->json('POST', '/api/auth/admin', [
            'email' => 'bad@email.com',
            'password' => '123456',
        ]);

        $response->assertExactJson([
            'message' => 'Invalid credentials',
        ]);
    }

    /**
     * Test succeed authentication for customer
     *
     * @return void
     * @test
     */
    public function customer_auth_succeed()
    {
        $password = '123456';

        $customer = factory(Customer::class)->create([
            'password' => $password,
        ]);

        $response = $this->json('POST', '/api/auth/customer', [
            'email' => $customer->email,
            'password' => $password,
        ]);

        $response->assertStatus(200);
    }
}
