<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Product;
use App\Models\Admin;
use App\Models\Customer;

class ProductsControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test listing method for products
     *
     * @return void
     * @test
     */
    public function index_should_return_products_list()
    {
        $products = factory(Product::class, 4)->create();

        $response = $this->json('GET', '/api/products');

        $response->assertJsonStructure([
            'data' => [],
            'meta' => [
                'last_page',
                'current_page',
            ],
            'links',
        ]);
    }

    /**
     * Test product creation response format
     *
     * @return void
     * @test
     */
    public function store_should_response_with_format()
    {
        $product = factory(Product::class)->make();
        $admin = factory(Admin::class)->create([
            'password' => '123456',
        ]);

        $this->actingAs($admin, 'admin');

        $response = $this->json('POST', '/api/products', $product->toArray());

        $response->assertJsonFragment([
            'name' => $product->name,
            'description' => $product->description,
            'price' => $product->price,
            'quantity' => $product->quantity,
        ]);
    }

    /**
     * Test product creation storing in database
     *
     * @return void
     * @test
     */
    public function store_should_register_on_db()
    {
        $product = factory(Product::class)->make();
        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');

        $response = $this->json('POST', '/api/products', $product->toArray());

        $this->assertDatabaseHas('products', [
            'name' => $product->name,
            'description' => $product->description,
            'price' => $product->price,
            'quantity' => $product->quantity,
        ]);
    }

    /**
     * Test product creation for not logged in admin should fail
     *
     * @return void
     * @test
     */
    public function store_should_fail_for_non_admin()
    {
        $product = factory(Product::class)->make();
        $customer = factory(Customer::class)->create();

        $this->actingAs($customer, 'customer');

        $response = $this->json('POST', '/api/products', $product->toArray());

        $response->assertStatus(401);
    }

    /**
     * Test product show method format
     *
     * @return void
     * @test
     */
    public function show_should_match_format()
    {
        $product = factory(Product::class)->create();

        $response = $this->json('GET', "/api/products/{$product->id}");

        $response->assertExactJson($product->toArray());
    }

    /**
     * Test product update record on db
     *
     * @return void
     * @test
     */
    public function update_should_change_record_on_db()
    {
        $product = factory(Product::class)->create();
        $admin = factory(Admin::class)->create();
        $name = 'A testing product';
        $description = 'A testing description';
        $price = 100;

        $this->actingAs($admin, 'admin');

        $this->json('PUT', "/api/products/{$product->id}", [
            'name' => $name,
            'description' => $description,
            'price' => $price,
        ]);

        $this->assertDatabaseHas('products', [
            'name' => $name,
            'description' => $description,
            'price' => $price,
        ]);
    }

    /**
     * Test product delete record on db
     *
     * @return void
     * @test
     */
    public function delete_should_remove_record_on_db()
    {
        $product = factory(Product::class)->create();
        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');

        $this->json('DELETE', "/api/products/{$product->id}");

        $this->assertDatabaseMissing('products', $product->toArray());
    }

    /**
     * Test product decreases quantity on buy
     *
     * @return void
     * @test
     */
    public function buy_should_decrease_product_quantity()
    {
        $product = factory(Product::class)->create([
            'quantity' => 20,
        ]);
        $customer = factory(Customer::class)->create();
        $quantity = 10;

        $this->actingAs($customer, 'customer');

        $this->json('POST', "/api/products/{$product->id}/buy", [
            'quantity' => $quantity,
        ]);

        $this->assertDatabaseHas('products', [
            'name' => $product->name,
            'quantity' => $product->quantity - $quantity,
        ]);
    }

    /**
     * Test product creates order log on buy
     *
     * @return void
     * @test
     */
    public function buy_should_create_order()
    {
        $product = factory(Product::class)->create();
        $customer = factory(Customer::class)->create();

        $this->actingAs($customer, 'customer');

        $this->json('POST', "/api/products/{$product->id}/buy");

        $this->assertDatabaseHas('orders', [
            'quantity' => 1,
            'spent' => $product->price,
            'customer_id' => $customer->id,
            'product_id' => $product->id,
        ]);
    }
}
