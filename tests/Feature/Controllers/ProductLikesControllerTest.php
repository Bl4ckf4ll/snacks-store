<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Like;

class ProductLikesControllerTest extends TestCase
{
    /**
     * Test endpoint creates product like record
     *
     * @return void
     * @test
     */
    public function like_should_add_like_to_product()
    {
        $product = factory(Product::class)->create();
        $customer = factory(Customer::class)->create();

        $this->actingAs($customer, 'customer');

        $this->json('POST', "/api/products/{$product->id}/likes");

        $this->assertDatabaseHas('likes', [
            'customer_id' => $customer->id,
            'product_id' => $product->id,
        ]);
    }

    /**
     * Test endpoint adds up to likes count field on product
     *
     * @return void
     * @test
     */
    public function like_should_add_to_likes_count_field()
    {
        $product = factory(Product::class)->create();
        $customer = factory(Customer::class)->create();

        $this->actingAs($customer, 'customer');

        $this->json('POST', "/api/products/{$product->id}/likes");

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'likes_count' => 1,
        ]);
    }
}
