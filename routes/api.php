<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
   Route::post('admin', 'AuthController@admin');
   Route::post('customer', 'AuthController@customer');
});

Route::group(['prefix' => 'products'], function () {
   Route::get('/', 'ProductsController@index');
   Route::get('/{product}', 'ProductsController@show');
});

Route::group(['middleware' => 'auth:admin'], function () {
   Route::group(['prefix' => 'products'], function () {
      Route::post('/', 'ProductsController@store');
      Route::put('/{product}', 'ProductsController@update');
      Route::patch('/{product}/{field}', 'ProductsController@update');
      Route::delete('{product}', 'ProductsController@delete');
   });
});

Route::group(['middleware' => 'auth:customer'], function () {
   Route::group(['prefix' => 'products/{product}'], function () {
      Route::post('likes', 'ProductLikesController@store');
      Route::post('buy', 'ProductsController@buy');
   });
});
