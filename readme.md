# Snacks Store
A simple Snacks Store app with a great API!

## Requisites
You need this stuff to run this API locally!

### With Docker
- Git
- Docker
- Docker Compose

### Without Docker

- Git
- PHP ~7.3
- MySQL ~5.7
- Composer ~1.8

## Setup
First, you need to clone the project on your computer,

So, open a terminal and run the next command:

```bash
git clone https://gitlab.com/Bl4ckf4ll/snacks-store.git
```

Then `cd` to the directory and then, there are two ways to run the project, with Docker and docker compose and Without them.

### With Docker and docker compose

With Docker, you just need to run the next command:

```bash
docker-compose up
```

And the API is ready to receive requests at [http://localhost:8000](http://localhost:8000), but WAIT! there's no content yet!

If you want some content, you need to attach to the docker container with the next command:

```bash
docker exec -it snacksstore_php_1 bash
```

Then, you can jump to [Database and dummy content setup](#database-and-dummy-content-setup)

### Without Docker

First, install composer packages:

```bash
composer install
```

Then, start the local server:

```
php artisan serve
```

Now go to the next section to know how to setup the database and some dummy content to use the API.

## Database and dummy content setup

First, copy .env.example to .env

```
cp .env.example .env
```

Run database migrations:

```bash
php artisan migrate
```

Then, to add a default Admin and Customer users, run the next command:

```bash
php artisan db:seed
```

You will get an output that will look like this:

```
Seeding: AdminsTableSeeder
Admin scarlett.schroeder@example.org
Admin dolly.padberg@example.com
Admin dibbert.maureen@example.com
Admin torphy.dylan@example.org
Admin mabelle35@example.com
Admin pagac.hailie@example.org
Admin goodwin.davon@example.org
Admin becker.cade@example.com
Admin shyanne.wiegand@example.com
Admin padberg.kody@example.org
Seeding: CustomersTableSeeder
Customer cecile25@example.com
Customer mae.price@example.com
Customer cleuschke@example.com
Customer bconn@example.net
Customer wisoky.evangeline@example.net
Customer klocko.kaia@example.com
Customer qcartwright@example.org
Customer thompson.milford@example.org
Customer naomi04@example.net
Customer carlo.mclaughlin@example.org
Seeding: ProductsTableSeeder
Database seeding completed successfully.
```

Then, you can use the emails in there to authenticate in the API, all of them have the same password `123456`,

And that's pretty much it, you have setup the API on your local machine!

## Testing

For testing create a .env.testing and setup the variables for the test database and run:

```bash
vendor/bin/phpunit
```