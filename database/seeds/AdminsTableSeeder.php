<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Admin::class, 10)
            ->create([
                'password' => '123456',
            ])
            ->each(function ($admin) {
                echo "Admin $admin->email\n";
            });
    }
}
