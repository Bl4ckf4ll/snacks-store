<?php

use Illuminate\Database\Seeder;
use App\Models\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Customer::class, 10)
            ->create([
                'password' => '123456',
            ])
            ->each(function ($customer) {
                echo "Customer $customer->email\n";
            });
    }
}
