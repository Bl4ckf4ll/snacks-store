<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Admin::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => str_random(),
    ];
});
