<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
    $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
    
    return [
        'name' => $faker->unique()->productName,
        'description' => $faker->text,
        'price' => random_int(100, 10000),
        'quantity' => random_int(10, 100),
        'likes_count' => 0,
    ];
});
